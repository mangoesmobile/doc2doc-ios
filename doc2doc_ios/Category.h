//
//  Search.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 4/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "MyManager.h"

/**
 Fetch Keywords and User_ids. Tips: Add in a new NSOperationQueue.
 */

@interface Category : NSOperation{
    MyManager *manager;

}

-(Category *)   init;
-(NSString *)   get_full_list;

@end
