//
//  Cases.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 28/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "Cases.h"

@implementation Cases

-(Cases *)init {
    return self;
}

-(NSString *)   case_details    :(NSString *) _case_id latest_reply_id :(NSString *)_latest_reply_id{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSString stringWithFormat:@"%@/cases/case_details", manager.url];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:_case_id forKey:@"case_id"];
    [request addPostValue:_case_id forKey:@"latest_reply_id"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}

-(NSString *)   fetch_followed  :(NSString *)_page_no{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    NSLog(@"%@", token);
    
    
    NSURL *url = [NSString stringWithFormat:@"%@/cases/fetch_followed", manager.url];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:_page_no forKey:@"page_no"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}



-(NSString *)   fetch_unfollowed:(NSString *)_page_no{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    NSLog(@"%@", token);
    
    
    NSURL *url = [NSString stringWithFormat:@"%@/cases/fetch_unfollowed", manager.url];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:_page_no forKey:@"page_no"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}

-(NSString *)   post_new        :(NSString *)_subject   contents        :(NSString *)_contents  geo_longitude   :(NSString *)_geo_longitude    geo_latitude    :(NSString *)_geo_latitude notify_user_ids :(NSString *)_notify_user_ids tag_category_ids:(NSString *)_tag_category_ids{
    
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSString stringWithFormat:@"%@/cases/post_new", manager.url];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:_subject forKey:@"subject"];
    [request addPostValue:_contents forKey:@"contents"];
    [request addPostValue:_geo_longitude forKey:@"geo_longitude"];
    [request addPostValue:_geo_latitude forKey:@"geo_latitude"];
    [request addPostValue:_notify_user_ids forKey:@"tag_user_ids"];
    [request addPostValue:_tag_category_ids forKey:@"tag_keywords"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        //NSLog(@"%@", response);
        
        //NSLog(@"%@",response);
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
    
}

-(NSString *)   post_reply      :(NSString *)_case_id contents        :(NSString *)_contents{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSString stringWithFormat:@"%@/cases/post_new", manager.url];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:_case_id forKey:@"subject"];
    [request addPostValue:_contents forKey:@"contents"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        //NSLog(@"%@", response);
        
        //NSLog(@"%@",response);
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}

-(NSString *)   start_following :(NSString *)_case_id{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSString stringWithFormat:@"%@/cases/start_following", manager.url];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:_case_id forKey:@"case_id"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        //NSLog(@"%@", response);
        
        //NSLog(@"%@",response);
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}

-(NSString *)   stop_following  :(NSString *)_case_id{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSString stringWithFormat:@"%@/cases/stop_following", manager.url];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:_case_id forKey:@"case_id"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        //NSLog(@"%@", response);
        
        //NSLog(@"%@",response);
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}


@end
