//
//  Cases.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 28/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "MyManager.h"

/**
 Fetch Cases. 
 Add in a new NSOperationQueue.
 */

@interface Cases : NSOperation{
    MyManager *manager;
}

-(Cases *)init;


-(NSString *)   case_details    :(NSString *) _case_id
                latest_reply_id :(NSString *)_latest_reply_id;

-(NSString *)   fetch_followed  :(NSString *)_page_no;

-(NSString *)   fetch_unfollowed:(NSString *)_page_no;

-(NSString *)   post_new        :(NSString *)_subject   
                contents        :(NSString *)_contents  
                geo_longitude   :(NSString *)_geo_longitude    
                geo_latitude    :(NSString *)_geo_latitude 
                notify_user_ids :(NSString *)_notify_user_ids
                tag_category_ids:(NSString *)_tag_category_ids; 

-(NSString *)   post_reply      :(NSString *)_case_id
                contents        :(NSString *)_contents ;

-(NSString *)   start_following :(NSString *)_case_id;

-(NSString *)   stop_following  :(NSString *)_case_id;

@end
