//
//  MPTopBarView.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyManager.h"
#import "SBJson.h"

//@protocol MMTopBarDelegate
////- (NSString *) setProfilePic;
//- (UIImage *) setProfilePic:(NSNotification *)notification;
//@end

@interface MMTopBarView : UIView{
    UIImageView *topBar;
    UIImageView *profile_pic_container;
    UIImageView *message_center;
    UIImageView *profile_picture;
    
    UIButton *onCall;
    UIButton *inBox;
    
    UIActivityIndicatorView *activity;

    int topBarStart;
    UILabel *user_name;
    UIImageView *location_pin;
    UILabel *location;
    UILabel *status;
    
    //id <MMTopBarDelegate> delegate;
}

@property(nonatomic, retain) UIImageView *profile_pic_container;
@property(nonatomic, retain) UIImageView *profile_picture;
@property(nonatomic, retain) UIImageView *message_center;
//@property(retain) id <MMTopBarDelegate> delegate;

-(void) refresh;
//-(void) getProfilePic;
-(void) moveMessageCenter;

@end
