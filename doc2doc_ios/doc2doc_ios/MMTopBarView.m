//
//  MPTopBarView.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "MMTopBarView.h"

@implementation MMTopBarView
@synthesize profile_pic_container, message_center, profile_picture;

//, delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        topBarStart= 270;
        self.userInteractionEnabled= YES;
        self.backgroundColor= [UIColor clearColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(profile_pic_problem:) 
                                                     name:@"TestNotification"
                                                   object:nil];
    }
    return self;
}

-(void) loadView{
    [self refresh];
}
-(void) refresh{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    topBar= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top_bar_bg"]];
    topBar.tag=0;
    topBar.frame = CGRectMake(0, 0, 320, 74);
    [self addSubview:topBar];
    
    profile_pic_container = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pro_pic_holder"]];
    profile_pic_container.frame = CGRectMake(0, 0, 74, 74);
    [topBar addSubview:profile_pic_container];
    
    message_center= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"message_center"]];
    message_center.frame = CGRectMake(320-50, 10, 50, 62);    
    [topBar addSubview:message_center];
    
    profile_picture = [[UIImageView alloc] initWithImage:nil];
    profile_picture.frame = CGRectMake(3 , 3 , 68, 68);
    profile_picture.backgroundColor= [UIColor clearColor];
    
    [profile_pic_container addSubview: activity];
    [profile_pic_container addSubview: profile_picture];
    [activity startAnimating];
//    [self getProfilePic];
    
    user_name = [[UILabel alloc] initWithFrame:CGRectMake(80, 20, 150, 25)];
    user_name.text = @"X. Yowzer";
    user_name.font= [UIFont fontWithName:@"times new roman" size: 25];
    user_name.backgroundColor = [UIColor clearColor];
    [topBar addSubview:user_name];
    
    location_pin = [[UIImageView alloc]initWithFrame:CGRectMake(90, 45, 9, 12)];
    location_pin.image= [UIImage imageNamed:@"location_pin"];
    [topBar addSubview:location_pin];
    
    location = [[UILabel alloc] initWithFrame:CGRectMake(100, 45, 100, 13)];
    location.text = @"location";
    location.font= [UIFont fontWithName:@"times new roman" size:12];
    location.backgroundColor = [UIColor clearColor];
    [topBar addSubview: location];
    
    status = [[UILabel alloc] initWithFrame:CGRectMake(180, 10, 100, 13)];
    status.text = @"online";
    status.font= [UIFont fontWithName:@"times new roman" size:12];
    status.backgroundColor = [UIColor clearColor];
    [topBar addSubview: status];
}

//-(void)getProfilePic{
//    UIImage *profile_pic=[[self delegate] setProfilePic:nil];
//    profile_picture.image= profile_pic;
//}

-(void) moveMessageCenter{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    if(message_center.frame.origin.x == 270){
        topBarStart = 120;
        message_center.frame= CGRectMake(topBarStart, 10, 50, 62); 
        
    }
    else{
        topBarStart= 270;
        message_center.frame= CGRectMake(topBarStart, 10, 50, 62); 
    } 
    
    [UIView commitAnimations];
}


- (void)handleTouchAtLocation:(CGPoint)touchLocation {
    NSLog(@"%@", NSStringFromCGPoint(touchLocation));
    
    if (touchLocation.x > message_center.frame.origin.x && touchLocation.x< message_center.frame.origin.x+50) {
        [self moveMessageCenter];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

-(void)profile_pic_problem:(NSNotification *)notification{
    [activity stopAnimating];
    [activity hidesWhenStopped];
    
    UIImage * image;
    if([[notification name] isEqualToString:@"TestNotification"]){
        NSString *vcard= [notification object];
        NSLog(@"%@", vcard);
        
        NSDictionary *raw_response= [vcard JSONValue];
        NSDictionary *response= [raw_response objectForKey:@"RESPONSE"];
        NSString *profile_pic=[response objectForKey:@"profile_pic_file_path"];
        NSLog(@"%@", profile_pic);
        
        NSString *ppic= [NSString stringWithFormat:@"%@/%@",@"http://50.18.120.168/doc2doc-BETA",profile_pic];
        NSLog(@"%@", ppic);
        
        NSURL * imageURL = [NSURL URLWithString:ppic];
        NSData * imageData = [NSData dataWithContentsOfURL:imageURL];
        image = [UIImage imageWithData:imageData];
    }
    
    profile_picture.image= image;
    
}
@end
