//
//  BoardViewController.m
//  doc2doc_ios
//
//  Created by Mahbub Morhsed on 31/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "BoardViewController.h"

@implementation BoardViewController
@synthesize myCases, pulseLikeTable, caseTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden =YES;
    
	// Do any additional setup after loading the view, typically from a nib.
    //[self setProfilePic];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate]; 
    [appDelegate.topBar refresh];
    
    PulseCopy *pulse= [[PulseCopy alloc]init];
    ThreadTableView *threadTable=[[ThreadTableView alloc]init];
    
    pulseLikeTable.delegate =pulse ;
    pulseLikeTable.dataSource= pulse;
    
    
    caseTableView.delegate = threadTable;
    caseTableView.dataSource= threadTable;
    
    self.pulseLikeTable.transform = CGAffineTransformMakeRotation(0);
    
    self.pulseLikeTable.transform = CGAffineTransformMakeRotation(-M_PI * 0.5);
    self.pulseLikeTable.backgroundColor= [UIColor clearColor];
    self.pulseLikeTable.rowHeight = 110;
    self.pulseLikeTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.pulseLikeTable.transform = CGAffineTransformTranslate( self.pulseLikeTable.transform, 117.0, 0.0);
    
    self.caseTableView.separatorStyle= UITableViewCellSeparatorStyleNone;
    self.caseTableView.rowHeight= 55;
    self.caseTableView.showsVerticalScrollIndicator= NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshTop:) 
                                                 name:@"TestNotification"
                                               object:nil];
}


-(void)refreshTop:(NSNotification *)notification{
    
    if([[notification name] isEqualToString:@"TestNotification"]){
        NSString *vcard= [notification object];
        NSLog(@"%@", vcard);
        
        NSDictionary *raw_response= [vcard JSONValue];
        NSDictionary *response= [raw_response objectForKey:@"RESPONSE"];
        NSString *profile_pic=[response objectForKey:@"profile_pic_file_path"];
        NSLog(@"%@", profile_pic);
        
        MyManager *man= [[MyManager alloc]init];
        NSString *ppic= [NSString stringWithFormat:@"%@/doc2doc-BETA/%@",man.url,profile_pic];
        NSLog(@"%@", ppic);
        
        NSURL * imageURL = [NSURL URLWithString:ppic];
        NSData * imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage * image = [UIImage imageWithData:imageData];
        NSLog(@"%@", image);
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];  
    [pulseLikeTable release];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [myCases setNeedsDisplay];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(IBAction)aMethod:(id)sender{    
    UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"I am now alerting you" message:@"Alert!! Alert!!" delegate:self cancelButtonTitle:@"thanks" otherButtonTitles:@"thanks", nil];
    [alert show];
}

-(void)refresh: (NSString *)threads{
    
    UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"Threads" message:threads delegate:self cancelButtonTitle:@"thanks" otherButtonTitles:@"thanks", nil];
    [alert show];
    
}

#pragma mark - MMTopBarView delegate
//-(NSString *)setProfilePic{    
//    return nil;
//}
@end
