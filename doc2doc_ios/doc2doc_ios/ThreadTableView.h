//
//  ThreadTableView.h
//  doc2doc_ios
//
//  Created by Mahbub Morhsed on 19/4/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"

@interface ThreadTableView : UITableViewController

@end
