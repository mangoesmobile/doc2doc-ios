//
//  PulseCopy.h
//  doc2doc_ios
//
//  Created by Mahbub Morhsed on 16/4/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"

@interface PulseCopy : UITableViewController{

    NSMutableArray *firstArray;
    NSMutableArray *secondArray;
}

@property(copy)    NSMutableArray *firstArray;
@property(copy)    NSMutableArray *secondArray;
@end
