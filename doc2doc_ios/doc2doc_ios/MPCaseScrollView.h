//
//  MPCaseScrollView.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPCaseScrollView : UIView{

    UIScrollView *caseScroll;
}

-(void)refresh;
-(void)removeTileFromView:(int)tileNo;

@end
