//
//  AppDelegate.h
//  doc2doc_ios
//
//  Created by Mahbub Morhsed on 31/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BoardViewController.h"

#import "TeamViewController.h"

#import "NewCaseViewController.h"

#import "AppsViewController.h"

#import "SettingsViewController.h"

#import "LoginViewController.h"

#import "MMTopBarView.h"

#import "TestFlight.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>{

    MMTopBarView *topBar;
    
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@property (strong, nonatomic)  MMTopBarView *topBar;

-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage;
-(void) customizeAndCreateTabBar;
-(void) addLoginPageOnTopOfTabBar;
-(void) addTopBarOnEachPage;
-(UIImage *)setProfilePic:(NSNotification *)notification;

@end
