//
//  LoginViewController.m
//  doc2doc_ios
//
//  Created by Mahbub Morhsed on 8/4/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "LoginViewController.h"

@implementation LoginViewController
@synthesize Scroll, login, password, top, loginButton, offlineButton, activity, image;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        Scroll                  = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
        Scroll.pagingEnabled    = NO;
        
        Scroll.contentSize      = CGSizeMake(self.view.frame.size.width , self.view.frame.size.height+100);
        
        [self.view addSubview:Scroll];
        [Scroll addSubview:login];
        [Scroll addSubview:password];
        [Scroll addSubview:top];
        [Scroll addSubview:loginButton];
        [Scroll addSubview:offlineButton];
        
        [Scroll release];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    activity.hidden  = YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == login) {
        [login resignFirstResponder];
    }
    else
        [password resignFirstResponder];
    return YES;
} 

-(void) load_vcard{
    
    User *us    = [[User alloc] init];
    vcard       = [us my_vcard];
    
    if(![vcard isEqualToString:@""]){
        NSLog(@"%@", vcard);
    }
    
    [[NSNotificationCenter defaultCenter] 
     postNotificationName:@"TestNotification" 
     object:vcard];

}

-(void) appLogin{
    
    Aunthenticate *auth= [[Aunthenticate alloc]init];
    
    NSString *result=[auth loginId: login.text password: password.text];
    
    if(![result isEqual:@"failure"]){
        [self performSelectorInBackground:@selector(load_vcard) withObject:nil];
        
        [activity stopAnimating];
        [self dismissModalViewControllerAnimated:NO];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please try again" delegate:self cancelButtonTitle:@"close" otherButtonTitles: nil];
        
        [alert show];
    }
}



-(IBAction)hide:(id)sender{
    activity.hidden = NO;
    [activity startAnimating];
    
    [NSUserDefaults resetStandardUserDefaults];
    
    [self performSelectorInBackground:@selector(appLogin) withObject:nil];
    
    activity.hidden = NO;
    
    // Let the device know we want to receive push notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
}

@end
