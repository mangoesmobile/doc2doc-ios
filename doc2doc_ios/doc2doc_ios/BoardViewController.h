//
//  FirstViewController.h
//  doc2doc_ios
//
//  Created by Mahbub Morhsed on 31/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPCaseScrollView.h"
#import "MMTopBarView.h"
#import "AppDelegate.h"
#import "User.h"
#import "PulseCopy.h"
#import "QuartzCore/QuartzCore.h"
#import "ThreadTableView.h"
#import "MyManager.h"

@interface BoardViewController : UIViewController{
    MPCaseScrollView *myCases;
    UITableView *pulseLikeTable;
    UITableView *caseTableView;
    
}

@property(nonatomic, retain) MPCaseScrollView *myCases;
@property(nonatomic, retain) IBOutlet UITableView *pulseLikeTable;
@property(nonatomic, retain) IBOutlet UITableView *caseTableView;

-(IBAction)aMethod:(id)sender;
-(void)refresh: (NSString *) threads;
-(void)refreshTop:(NSNotification *)notification;

@end
