//
//  LoginViewController.h
//  doc2doc_ios
//
//  Created by Mahbub Morhsed on 8/4/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Aunthenticate.h"

#import "User.h"

#import "User.h"

#import "BoardViewController.h"

#import "SBJson.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate, UIScrollViewDelegate>{
    
    UIScrollView *Scroll;
    
    UIImageView *top;
    UITextField *login;
    UITextField *password;
    
    UIButton *loginButton;
    UIButton *offlineButton;
    
    UIActivityIndicatorView *activity;
    
    UIImageView *image;
    
    NSString *vcard;
}

@property(nonatomic, retain) IBOutlet UIImageView *image;

@property(nonatomic, retain) IBOutlet UIScrollView *Scroll;
@property(nonatomic, retain) IBOutlet UITextField *login;
@property(nonatomic, retain) IBOutlet UITextField *password;
@property(nonatomic, retain) IBOutlet UIImageView *top;

@property(nonatomic, retain) IBOutlet UIButton *loginButton;
@property(nonatomic, retain) IBOutlet UIButton *offlineButton;

@property(nonatomic, retain) IBOutlet UIActivityIndicatorView *activity;





//-(IBAction) loginToDoc2Doc:(id)sender;
//
//@end




-(IBAction)hide:(id)sender;
@end
