//
//  MPCaseScrollView.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "MPCaseScrollView.h"

@implementation MPCaseScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor= [UIColor clearColor];
        self.opaque= YES;
        
        [self refresh];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 
 //CGContextRef context = UIGraphicsGetCurrentContext();
 self.clearsContextBeforeDrawing = NO; 
 
 caseScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,self.bounds.size.width, 72)];
 
 //    caseScroll = [[UIScrollView alloc] initWithFrame:myFrame];
 caseScroll.pagingEnabled = NO;
 
 //caseScroll.backgroundColor = [UIColor clearColor];
 
 caseScroll.contentSize = CGSizeMake(72*10 + 5*10, 72);
 [self addSubview:caseScroll];
 
 caseScroll.showsHorizontalScrollIndicator= NO;
 for (int i=0 ; i<10;i++){
 UIImageView *sample= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tile.png"]];
 sample.tag= i;
 sample.frame = CGRectMake(i*72+i*5, 0, 72, 72);
 [caseScroll addSubview:sample];
 }
 [caseScroll release];
 }*/


- (void)dealloc {
    
    [caseScroll release];
    // Call the inherited implementation
    [super dealloc];
}

-(void)refresh{
    
    NSLog(@"In MPCaseScrollView");
    
    [[self viewWithTag:0] removeFromSuperview];
    
    caseScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,self.bounds.size.width, 91)];
    caseScroll.tag= 0;
    
    //    caseScroll = [[UIScrollView alloc] initWithFrame:myFrame];
    caseScroll.pagingEnabled = NO;
    
    //caseScroll.backgroundColor = [UIColor clearColor];
    
    caseScroll.contentSize = CGSizeMake(103*10 + 5*10, 77);
    [self addSubview:caseScroll];
    
    caseScroll.showsHorizontalScrollIndicator= NO;
    for (int i=1 ; i<10;i++){
        UIImageView *sample= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tile.png"]];
        sample.tag= i;
        sample.frame = CGRectMake((i-1)*103+(i-1)*5, 0, 103, 77);
        [caseScroll addSubview:sample];
    }
    [caseScroll release];
    
    //[self removeTileFromView:4];
}

-(void)removeTileFromView:(int)tileNo{
    [[self viewWithTag:tileNo] removeFromSuperview];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    //NSSet *allTouches = [event allTouches]; 
    for (UITouch *touch in touches) 
    { 
        CGPoint location = [touch locationInView:touch.view];
        [self removeTileFromView: touch.view.tag  ];
        NSLog(@"%@", location);
    }
    
}


- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    
}

-(void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    
}



@end
