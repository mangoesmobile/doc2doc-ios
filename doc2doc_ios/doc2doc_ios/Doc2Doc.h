//
//  Doc2Doc.h
//  doc2doc_ios
//
//  Created by Mahbub Morhsed on 24/4/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Aunthenticate.h"
#import "Category.h"
#import "Cases.h"
#import "Notification.h"
#import "Location.h"
#import "Message.h"
#import "Ping.h"
#import "User.h"

@interface Doc2Doc : NSObject{
    NSString *access_token;
}
@end
