//
//  AppDelegate.m
//  doc2doc_ios
//
//  Created by Mahbub Morhsed on 31/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "AppDelegate.h"



@implementation AppDelegate

@synthesize window = _window;
@synthesize tabBarController = _tabBarController;
@synthesize topBar=_topBar;

- (void)dealloc
{
    [_window release];
    [_tabBarController release];
    [super dealloc];
}
/*
 My Apps Custom uncaught exception catcher, we do special stuff here, and TestFlight takes care of the rest
 **/
void HandleExceptions(NSException *exception) {
    NSLog(@"This is where we save the application data during a exception");
    // Save application data on crash
}
/*
 My Apps Custom signal catcher, we do special stuff here, and TestFlight takes care of the rest
 **/
void SignalHandler(int sig) {
    NSLog(@"This is where we save the application data during a signal");
    // Save application data on crash
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    // installs HandleExceptions as the Uncaught Exception Handler
    NSSetUncaughtExceptionHandler(&HandleExceptions);
    // create the signal action structure 
    struct sigaction newSignalAction;
    // initialize the signal action structure
    memset(&newSignalAction, 0, sizeof(newSignalAction));
    // set SignalHandler as the handler in the signal action structure
    newSignalAction.sa_handler = &SignalHandler;
    // set SignalHandler as the handlers for SIGABRT, SIGILL and SIGBUS
    sigaction(SIGABRT, &newSignalAction, NULL);
    sigaction(SIGILL, &newSignalAction, NULL);
    sigaction(SIGBUS, &newSignalAction, NULL);
    // Call takeOff after install your own unhandled exception and signal handlers
    
    [TestFlight takeOff:@"8a166d1246897a28f1b520e1073f885b_Mjg2NjEyMDExLTA5LTE0IDExOjE0OjMzLjMxMjA4Nw"];

    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
    [self customizeAndCreateTabBar];
    
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    
    [self addLoginPageOnTopOfTabBar];
    [self addTopBarOnEachPage];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setProfilePic:) 
                                                 name:@"TestNotification"
                                               object:nil];
    
         
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

-(void)customizeAndCreateTabBar{
    
    NSMutableArray *localControllers = [[NSMutableArray alloc] initWithCapacity:5];
    
    UINavigationController *navViewController1 = [[UINavigationController alloc] initWithNibName:nil bundle:nil];    
    UIViewController *viewController1 = [[[BoardViewController alloc] initWithNibName:@"BoardViewController" bundle:nil] autorelease];
    [navViewController1 pushViewController:viewController1 animated:NO];
    [[navViewController1 tabBarItem] setTitle:@"Board"];
    navViewController1.tabBarItem.image = [UIImage imageNamed:@"icon1_board"];
    [localControllers addObject:navViewController1];
    
    UINavigationController *navViewController2 = [[UINavigationController alloc] initWithNibName:nil bundle:nil];    
    UIViewController *viewController2 = [[[TeamViewController alloc] initWithNibName:@"TeamViewController" bundle:nil] autorelease];
    [navViewController2 pushViewController:viewController2 animated:NO];
    [[navViewController2 tabBarItem] setTitle:@"Team"];
    navViewController2.tabBarItem.image = [UIImage imageNamed:@"icon2_team"];
    [localControllers addObject:navViewController2];
    
    UINavigationController *navViewController3 = [[UINavigationController alloc] initWithNibName:nil bundle:nil];    
    UIViewController *viewController3 = [[[NewCaseViewController alloc] initWithNibName:@"NewCaseViewController" bundle:nil] autorelease];
    [navViewController3 pushViewController:viewController3 animated:NO];
    [[navViewController3 tabBarItem] setTitle:@"New Case"];
    navViewController3.tabBarItem.image = [UIImage imageNamed:@"icon3_new_case"];
    [localControllers addObject:navViewController3];
    
    UINavigationController *navViewController4 = [[UINavigationController alloc] initWithNibName:nil bundle:nil];    
    UIViewController *viewController4 = [[[AppsViewController alloc] initWithNibName:@"AppsViewController" bundle:nil] autorelease];
    [navViewController4 pushViewController:viewController4 animated:NO];
    [[navViewController4 tabBarItem] setTitle:@"Apps"];
    navViewController4.tabBarItem.image = [UIImage imageNamed:@"icon4_apps"];
    [localControllers addObject:navViewController4];
    
    UINavigationController *navViewController5 = [[UINavigationController alloc] initWithNibName:nil bundle:nil];    
    UIViewController *viewController5 = [[[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil] autorelease];
    [navViewController5 pushViewController:viewController5 animated:NO];
    [[navViewController5 tabBarItem] setTitle:@"Settings"];
    navViewController5.tabBarItem.image = [UIImage imageNamed:@"icon5_settings"];
    [localControllers addObject:navViewController5];
    
    
    self.tabBarController = [[[UITabBarController alloc] init] autorelease];
    
    [self.tabBarController setViewControllers: localControllers animated:NO];
    self.tabBarController.tabBar.backgroundImage=[UIImage imageNamed:@"tab_bar_bg"];
    
    UIImageView *tabBarNipple= [[UIImageView alloc]initWithFrame:CGRectMake(21, -10, 18, 14)];
    tabBarNipple.tag= 123;
    tabBarNipple.image= [UIImage imageNamed:@"tab_bar_nipple"];
    [self.tabBarController.tabBar addSubview:tabBarNipple];
    self.tabBarController.delegate =self;
    
    [self addCenterButtonWithImage:[UIImage imageNamed:@"icon3_new_case"] highlightImage:[UIImage imageNamed:@"icon3_new_case"]];
}

-(void) addLoginPageOnTopOfTabBar{
    LoginViewController *login= [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
    [self.tabBarController presentModalViewController:login animated:NO];
    [login release];
}


// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    int selected= tabBarController.selectedIndex;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    
    switch (selected) {
        case 0:{
            UIView *tabBarNipple= [self.tabBarController.tabBar viewWithTag:123];
            tabBarNipple.frame =CGRectMake(21, -10, 18, 14);
            break;
        }
        case 1:{
            UIView *tabBarNipple= [self.tabBarController.tabBar viewWithTag:123];
            tabBarNipple.frame =CGRectMake(86, -10, 18, 14);
            topBar.backgroundColor= [UIColor blackColor];
            break;
        }
        case 2:{
            UIView *tabBarNipple= [self.tabBarController.tabBar viewWithTag:123];
            tabBarNipple.frame =CGRectMake(151, -10, 18, 14);
            break;
        }
        case 3:{
            UIView *tabBarNipple= [self.tabBarController.tabBar viewWithTag:123];
            tabBarNipple.frame =CGRectMake(151+65, -10, 18, 14);
            break;
        }
        case 4:{
            UIView *tabBarNipple= [self.tabBarController.tabBar viewWithTag:123];
            tabBarNipple.frame =CGRectMake(151+130, -10, 18, 14);
            break;
        }
            
        default:
            break;
    }
    
    [UIView commitAnimations];
    
}



/*
 
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
 {
 NSLog(@"%@",@"changed");
 
 if(tabBarController.selectedIndex ==1){
 UIView *tabBarNipple= [self.tabBarController.tabBar viewWithTag:123];
 tabBarNipple.frame =CGRectMake(21, -10, 18, 14);     
 }
 
 }
 
 */

-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage
{
    //UIImageView *buttonImage= [UIImageView alloc ]initWithImage:[UIImage imageNamed: @"icon3_newcase.png"];
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    button.frame = CGRectMake(0.0, 0.0, 46, 46);
//    [button setBackgroundImage:buttonImage  forState:UIControlStateNormal];
//    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    [button addTarget:self.tabBarController action:@selector(selectCase) forControlEvents:UIControlEventTouchUpInside];
    
//    CGFloat heightDifference = 46 - self.tabBarController.tabBar.frame.size.height;
//    if (heightDifference < 0)
        button.center = self.tabBarController.tabBar.center;
//    else
//    {
//        CGPoint center = self.tabBarController.tabBar.center;
//        center.y = center.y - heightDifference/2.0;
//        button.center = center;
//    }
//    
    [self.tabBarController.tabBar addSubview:button];
}

-(void)selectCase{
    [self.tabBarController setSelectedIndex:2];
}

-(void)addTopBarOnEachPage{
    _topBar=[[MMTopBarView alloc] initWithFrame:CGRectMake(0, 20, 320, 86)];
    //BoardViewController *board=  [[BoardViewController alloc]init];
//    _topBar.delegate= self;
    [self.window.rootViewController.view addSubview:_topBar];
    [_topBar refresh];
    
}

-(UIImage *)setProfilePic:(NSNotification *)notification{
    
    UIImage * image;
    if([[notification name] isEqualToString:@"TestNotification"]){
        NSString *vcard= [notification object];
        NSLog(@"%@", vcard);
        
        NSDictionary *raw_response= [vcard JSONValue];
        NSDictionary *response= [raw_response objectForKey:@"RESPONSE"];
        NSString *profile_pic=[response objectForKey:@"profile_pic_file_path"];
        NSLog(@"%@", profile_pic);
        
        //MyManager *man= [[MyManager alloc]init];
        NSString *ppic= [NSString stringWithFormat:@"%@/%@",@"http://50.18.120.168/doc2doc-BETA",profile_pic];
        NSLog(@"%@", ppic);
        
        NSURL * imageURL = [NSURL URLWithString:ppic];
        NSData * imageData = [NSData dataWithContentsOfURL:imageURL];
        image = [UIImage imageWithData:imageData];
    }
    
    return image;
}
@end
