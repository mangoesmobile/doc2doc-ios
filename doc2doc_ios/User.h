//
//  User.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 29/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MyManager.h"
#import "ASIFormDataRequest.h"

/**
 for setting/getting user availability status, vCards, profile pictures etc
 */

@interface User : NSOperation{
    MyManager *manager;
}

-(User *) init;

-(NSString *) all_vcards;
-(NSString *) get_vcard;
-(NSString *) my_vcard;
-(NSString *) set_profile_pic;
-(NSString *) set_user_availability_status;


@end
