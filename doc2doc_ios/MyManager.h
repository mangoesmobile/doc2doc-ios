//
//  MyManager.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

/*
 Link to doc2doc-BETA manual: 
 http://50.18.120.168/doc2doc-BETA/manual/
*/

#import <foundation/Foundation.h>

@interface MyManager : NSObject {
    NSString *url;
}

@property (nonatomic, retain) NSString *url;

+ (id)sharedManager;

@end