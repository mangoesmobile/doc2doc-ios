//
//  Aunthenticate.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "Aunthenticate.h"

@implementation Aunthenticate


-(Aunthenticate *)init {
    
    manager= [[MyManager alloc] init];
    NSLog (@"%@", manager.url);

    return self;

}

- (NSString *) loginId:(NSString *)login password:(NSString *)password{
    
    //SHA1 Encryption code
    NSString *hashkey = password;
    const char *s = [hashkey cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
    uint8_t result[CC_SHA1_DIGEST_LENGTH] = {0};
    CC_SHA1(keyData.bytes, keyData.length, result);
    //SHA1 Encryption code

    NSString *pass = [NSString  stringWithFormat:
                   @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                   result[0], result[1], result[2], result[3], result[4],
                   result[5], result[6], result[7],
                   result[8], result[9], result[10], result[11], result[12],
                   result[13], result[14], result[15],
                   result[16], result[17], result[18], result[19]
                   ];
    
    NSString *returnString;
    
    NSString *urlString= [NSString stringWithFormat:@"%@/authenticate/login", manager.url];
    
    NSURL *url = [NSURL URLWithString:urlString];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue: login forKey:@"uname"];
    [request addPostValue: [pass lowercaseString ] forKey:@"enc_pword"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    
    
    
    if (!error) {
        NSString *response = [request responseString];
        

        NSDictionary *dictionary =  [response JSONValue];
        
        NSString *error_log= [dictionary objectForKey:@"ERROR_LOG"];
        NSDictionary *token      =  [dictionary objectForKey:@"RESPONSE"];
        NSString *access_token         =  [token objectForKey:@"access_token"] ;
        NSString *_status=[dictionary objectForKey:@"STATUS"];
        NSString *_statusMessage= [dictionary objectForKey:@"STATUS_MESSAGE"];
        
        NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
        [defaults setObject:access_token forKey:@"token"];
        [defaults synchronize];
        
        if ([_statusMessage isEqualToString:@"Logged in"]) {
            returnString= @"success";
        }
        else 
            returnString =@"failure";
    }
    
    return returnString;
}

-( NSString *) logOut{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token=[prefs objectForKey:@"token"];
    
    NSString *urlString= [NSString stringWithFormat:@"%@/authenticate/logout", manager.url];
    
    NSURL *url = [NSURL URLWithString:urlString];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request startSynchronous];
    NSError *error = [request error];
    NSString *response;
    
    if (!error) {
       response  = [request responseString];
        
    }
    return  response;
}

- (IBAction)sendRe:(id)sender
{
//    NSURL *url = [NSURL URLWithString:@"http://allseeing-i.com"];
//    
//    
//    __block ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
//    [request setCompletionBlock:^{
//        // Use when fetching text data
//        responseString = [request responseString];
//        
//        // Use when fetching binary data
//        NSData *responseData = [request responseData];
//    }];
//    [request setFailedBlock:^{
//        NSError *error = [request error];
//    }];
//    
//    [request startAsynchronous]; 
//    NSLog(@"%@", responseString);

}

@end
