//
//  User.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 29/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "User.h"

@implementation User

-(User *)init {
    
    manager = [[MyManager alloc] init];
    return self;
}

-(NSString *) fetch_inbox:(NSString *)page_no{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    NSString *urlString= [NSString stringWithFormat:@"%@/user/fetch_inbox", manager.url];
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:page_no forKey:@"page_no"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}


-(NSString *) all_vcards{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    NSString *urlString= [NSString stringWithFormat:@"%@/user/all_vcards", manager.url];
    
    NSURL *url = [NSURL URLWithString:urlString];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}


-(NSString *) get_vcard{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
   // NSLog(@"Inside get_vcard- %@", token);
    
    NSString *urlString= [NSString stringWithFormat:@"%@/user/get_vcard", manager.url];
    
    NSURL *url = [NSURL URLWithString:urlString];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:@"demo" forKey:@"user_id"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            returnString= [NSString stringWithFormat:@"%@", response];
        }
        else{

        }
    }
    else{

    }
    
    return returnString;
}


-(NSString *) my_vcard{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    // NSLog(@"Inside get_vcard- %@", token);
    
    NSString *urlString= [NSString stringWithFormat:@"%@/user/my_vcard", manager.url];
    
    NSURL *url = [NSURL URLWithString:urlString];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        returnString= [NSString stringWithFormat:@"%@", response];
        
        if(![response isEqualToString:@"FALSE"]){
            returnString= [NSString stringWithFormat:@"%@", response];
        }
        else{
            
        }
    }
    else{
        
    }
    
    return returnString;
}
-(NSString *) set_profile_pic{
    return @"incomplete";
}
-(NSString *) set_user_availability_status{
    return @"incomplete";
}

@end
