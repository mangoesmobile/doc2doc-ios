//
//  Search.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 4/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "Category.h"

@implementation Category

-(Category *)init {
    
    return self;
}

-(NSString *)   get_full_list{

    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token=[prefs objectForKey:@"token"];
    
    NSString *urlString= [NSString stringWithFormat:@"%@/category/get_full_list", manager.url];
    
    NSURL *url = [NSURL URLWithString:urlString];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request startSynchronous];
    NSError *error = [request error];
    NSString *response;
    
    if (!error) {
        response  = [request responseString];
    }
    return  response;
}

@end
