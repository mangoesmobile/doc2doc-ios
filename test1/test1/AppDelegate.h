//
//  AppDelegate.h
//  test1
//
//  Created by Mahbub Morhsed on 19/3/12.
//  Copyright Mangoes Mobile 2012. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

@end
