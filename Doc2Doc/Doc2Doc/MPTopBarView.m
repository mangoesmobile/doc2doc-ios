//
//  MPTopBarView.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "MPTopBarView.h"

@implementation MPTopBarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor= [UIColor clearColor];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    topBar= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top_bar.png"]];
    topBar.frame = CGRectMake(0, 0, 320, 74);
    [self addSubview:topBar];
    
    profile_pic = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pro_pic_holder.png"]];
    profile_pic.frame = CGRectMake(7, 13, 73, 73);
    
    [topBar addSubview:profile_pic];
    
    onCall = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 85, 29)];
}


@end
