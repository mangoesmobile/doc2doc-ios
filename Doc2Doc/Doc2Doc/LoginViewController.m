//
//  LoginViewController.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 13/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "LoginViewController.h"

@implementation LoginViewController
@synthesize Scroll, login, password, top, loginButton, offlineButton, activity, image, topBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.backgroundColor = [UIColor clearColor];
        Scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        Scroll.pagingEnabled = NO;
        
        Scroll.contentSize = CGSizeMake(self.view.frame.size.width , self.view.frame.size.height+100);
        [self.view addSubview:Scroll];
        
        [Scroll addSubview:login];
        [Scroll addSubview:password];
        [Scroll addSubview:top];
        [Scroll addSubview:loginButton];
        [Scroll addSubview:offlineButton];
        
        [Scroll release];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad


{
    
    [super viewDidLoad];
    
//    NSArray *nibViews= [[NSBundle mainBundle] loadNibNamed:@"topbarView" owner:self options:nil];
//    topBar= [nibViews objectAtIndex:0];
//    
//    [self.view addSubview: self.topBar];
    
    
    
    //[self.view bringSubviewToFront:image];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait|| interfaceOrientation ==UIInterfaceOrientationLandscapeLeft);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == login) {
            [login resignFirstResponder];
    }
    else
        [password resignFirstResponder];
    return YES;
}

-(IBAction) loginToDoc2Doc:(id)sender{
    
    [activity startAnimating];
    Aunthenticate *auth= [[Aunthenticate alloc]init];
    NSString *result=[auth loginId: login.text password: password.text];
    NSLog(@"Succesfully logged in -%@", result);
    [activity stopAnimating];
    
    [NSUserDefaults resetStandardUserDefaults];
    
    // Let the device know we want to receive push notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    
    MainScreen *sampleView = [[[MainScreen alloc] init] autorelease];
    //[sampleView setDelegate: self];
    
    [sampleView setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentModalViewController:sampleView animated:NO];
}



@end
