//
//  LoginViewController.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 13/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Aunthenticate.h"
#import "MainScreen.h"
#import "TopBarView.h"



@interface LoginViewController : UIViewController<UITextFieldDelegate>{

    UIScrollView *Scroll;
    
    UIImageView *top;
    UITextField *login;
    UITextField *password;
    
    UIButton *loginButton;
    UIButton *offlineButton;
    
    UIActivityIndicatorView *activity;
    
    
    UIImageView *image;
    

}

@property(nonatomic, retain) IBOutlet UIImageView *image;

@property(nonatomic, retain) IBOutlet UIScrollView *Scroll;
@property(nonatomic, retain) IBOutlet UITextField *login;
@property(nonatomic, retain) IBOutlet UITextField *password;
@property(nonatomic, retain) IBOutlet UIImageView *top;

@property(nonatomic, retain) IBOutlet UIButton *loginButton;
@property(nonatomic, retain) IBOutlet UIButton *offlineButton;

@property(nonatomic, retain) IBOutlet UIActivityIndicatorView *activity;

@property(nonatomic, retain) IBOutlet TopbarView *topBar;



-(IBAction) loginToDoc2Doc:(id)sender;

@end
