//
//  TopbarView.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 28/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "TopbarView.h"

@implementation TopbarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
