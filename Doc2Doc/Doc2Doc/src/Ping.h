//
//  Ping.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 4/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

@interface Ping : NSObject{

}

-(Ping *) init;

-(NSString *) read;
-(NSString *) send:(NSString *)recipient_id;

@end
