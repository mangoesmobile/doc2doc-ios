//
//  Aunthenticate.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

/**
 Facilitate authentication of user. Always call this in a new thread to avoid UI freezing. 
 */

#import <Foundation/Foundation.h>
#import "MyManager.h"
#import "ASIFormDataRequest.h"
#include <CommonCrypto/CommonDigest.h>
#import "ASIHTTPRequest.h"

@interface Aunthenticate : NSObject{

}


/**
 Initialize a new Authenticate object
 
 To initilize write the following code: "Aunthenticate *auth= [[Aunthenticate alloc]init];"
 */

-(Aunthenticate *)init;


/**
 Connects to the server and logs in the user it also retrieves the token and saves it. To further retrieve the token write the following code:
 
 "NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
 NSString *token=[prefs objectForKey:@"token"]; "
 
 @param loginId the login id/ User Name of the user
 @param password the password of the user 
 @returns False if login unsuccessful or if successful returns the token
 */

- (NSString *) loginId:(NSString *)login password:(NSString *)password;

/**
 Just call this method to log out. 
 */
- (NSString *) logOut;

@end
