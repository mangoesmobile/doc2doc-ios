//
//  User.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 29/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

@interface User : NSObject{

}

-(User *) init;

-(NSString *) fetch_inbox:(NSString *)page_no;
-(NSString *) fetch_sentbox:(NSString *)page_no;
-(NSString *) message_details:(NSString *)message_id;
-(NSString *) post_new;  
-(NSString *) post_reply;


@end
