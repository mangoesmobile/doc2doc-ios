//
//  User.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 29/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "User.h"

@implementation User

-(User *)init {
    return self;
}

-(NSString *) fetch_inbox:(NSString *)page_no{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/user/fetch_inbox"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:page_no forKey:@"page_no"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}
-(NSString *) fetch_sentbox:(NSString *)page_no{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/user/fetch_sentbox"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:page_no forKey:@"page_no"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}
-(NSString *) message_details:(NSString *)message_id{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/user/message_details"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:message_id forKey:@"message_id"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}

-(NSString *) post_new{
return @"incomplete";
}  
-(NSString *) post_reply{
return @"incomplete";
}

@end
