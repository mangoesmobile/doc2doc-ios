//
//  Thread.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 28/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "Thread.h"

@implementation Thread

-(Thread *)init {
    return self;
}

-(NSString *)fetchAll:(NSString *)page_no{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/thread/fetch_all"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:page_no forKey:@"page_no"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}

-(NSString *) fetch_my_threads:(NSString *)page_no{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/thread/fetch_my_threads"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:page_no forKey:@"page_no"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}

-(NSString *) post_new:(NSString *)subject post_content:(NSString *)contents longitude:(NSString *)geo_longitude latitude:(NSString *)geo_latitude user_ids:(NSString *)tag_user_ids tag_keywords:(NSString *)tag_keywords {
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/thread/post_new"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:subject forKey:@"subject"];
    [request addPostValue:contents forKey:@"contents"];
    [request addPostValue:geo_longitude forKey:@"geo_longitude"];
    [request addPostValue:geo_latitude forKey:@"geo_latitude"];
    [request addPostValue:tag_user_ids forKey:@"tag_user_ids"];
    [request addPostValue:tag_keywords forKey:@"tag_keywords"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        //NSLog(@"%@", response);
        
        //NSLog(@"%@",response);
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}


-(NSString *) post_reply:(NSString *)thread_id post_content:(NSString *)contents longitude:(NSString *)geo_longitude latitude:(NSString *)geo_latitude user_ids:(NSString *)tag_user_ids {
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/thread/post_reply"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:thread_id forKey:@"thread_id"];
    [request addPostValue:contents forKey:@"contents"];
    [request addPostValue:geo_longitude forKey:@"geo_longitude"];
    [request addPostValue:geo_latitude forKey:@"geo_latitude"];
    [request addPostValue:tag_user_ids forKey:@"tag_user_ids"];

    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
    
    return @"incomplete";
}

-(NSString *) thread_details:(NSString *)thread_id{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/thread/thread_details"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:thread_id forKey:@"thread_id"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        
        NSString *response = [request responseString];
      
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
    
}

@end
