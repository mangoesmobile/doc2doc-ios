//
//  Search.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 4/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

/**
 Fetch Keywords and User_ids. Tips: Add in a new NSOperationQueue.
 */

@interface Search : NSOperation{


}

-(Search *) init;
-(NSString *) keywords:(NSString *)keywords_hsv;
-(NSString *) users:(NSString *)user_ids_hsv;

@end
