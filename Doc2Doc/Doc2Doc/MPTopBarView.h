//
//  MPTopBarView.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPTopBarView : UIView{
    UIImageView *topBar;
    UIImageView *profile_pic;
    
    UIButton *onCall;
    UIButton *inBox;
}

@end
