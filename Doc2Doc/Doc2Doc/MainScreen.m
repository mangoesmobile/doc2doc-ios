//
//  MainScreen.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "MainScreen.h"


@implementation MainScreen
@synthesize atopBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    MPTopBarView *topBar=[[MPTopBarView alloc] initWithFrame:CGRectMake(0, 0, 320, 86)];
    MPCaseScrollView *myCases= [[MPCaseScrollView alloc] initWithFrame:CGRectMake(13, 99, 294, 72)];
    
    [self.view addSubview:topBar];
    
//    NSArray *nibViews= [[NSBundle mainBundle] loadNibNamed:@"topbarView" owner:self options:nil];
//    atopBar= [nibViews objectAtIndex:0];
//    
//    [self.view addSubview: self.atopBar];
    
    [self.view addSubview:myCases];
    
    [topBar setNeedsLayout];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
