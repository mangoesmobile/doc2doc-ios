//
//  Notification.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 4/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

/**
 Fetch new notifications. Tips: Add in a new NSOperationQueue.
 */

@interface Notification : NSOperation{

}

-(Notification *)init;
-(NSString *)get_all_new_counts;

@end
