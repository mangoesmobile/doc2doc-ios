//
//  MPCaseScrollView.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "MPCaseScrollView.h"

@implementation MPCaseScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor= [UIColor clearColor];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    caseScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,self.frame.size.width, 72)];
    caseScroll.pagingEnabled = NO;
    
    //caseScroll.backgroundColor = [UIColor clearColor];
    
    caseScroll.contentSize = CGSizeMake(72*10, 72);
    [self addSubview:caseScroll];
    
    caseScroll.showsHorizontalScrollIndicator= NO;
    for (int i=0 ; i<10;i++){
        
        UIImageView *sample= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tile.png"]];
        sample.frame = CGRectMake(i*72+i*5, 0, 72, 72);
        [caseScroll addSubview:sample];
    }
    [caseScroll release];
}


@end
