//
//  ApplicationTabbarController.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainScreen.h"
#import "LoginViewController.h"


@interface ApplicationTabBarController : UITabBarController {
    
}

-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage;

@end