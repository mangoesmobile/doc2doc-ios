//
//  MyManager.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <foundation/Foundation.h>

@interface MyManager : NSObject {
    NSString *url;
}

@property (nonatomic, retain) NSString *url;

+ (id)sharedManager;

@end