//
//  MainScreen.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPCaseScrollView.h"
#import "MPTopBarView.h"
#import "TopbarView.h"
#import "ApplicationTabbarController.h"

@interface MainScreen : UIViewController{
    
}

@property(nonatomic, retain) IBOutlet TopbarView *atopBar;


@end
