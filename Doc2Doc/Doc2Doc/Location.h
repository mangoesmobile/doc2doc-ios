//
//  Location.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 4/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

@interface Location : NSOperation{

}

-(Location *)init;

-(NSString *) set_my_location:(NSString *)longitude geo_latitude:(NSString *)latitude ;
-(NSString *) unset_my_location;

@end
