//
//  Message.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 4/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

/**
 Fetch Threads. Tips: Add in a new NSOperationQueue.
 */

@interface Message : NSOperation{

}

-(Message *)init;

-(NSString *) fetch_inbox:(NSString *)page_no;
-(NSString *) fetch_sentbox:(NSString *)page_no;
-(NSString *) message_details:(NSString *)message_id;
-(NSString *) post_new:(NSString *)subject contents:(NSString *)contents longitude:(NSString *)geo_longitude latitude:(NSString *)geo_latitude tag_user:(NSString *)tag_user_ids tag_keyword:(NSString *)tag_keywords;
-(NSString *) post_reply:(NSString *)message_id contents:(NSString *)contents longitude:(NSString *)geo_longitude latitude:(NSString *)geo_latitude;

@end
