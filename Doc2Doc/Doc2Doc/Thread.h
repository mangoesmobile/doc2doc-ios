//
//  Thread.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 28/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

/**
 Fetch Threads. Tips: Add in a new NSOperationQueue.
 */

@interface Thread : NSOperation{

}

-(Thread *)init;

-(NSString *) fetchAll:(NSString *)page_no;
-(NSString *) fetch_my_threads:(NSString *)page_no;

-(NSString *) post_new:(NSString *)subject post_content:(NSString *)contents longitude:(NSString *)geo_longitude latitude:(NSString *)geo_latitude user_ids:(NSString *)tag_user_ids tag_keywords:(NSString *)tag_keywords ;

-(NSString *) post_reply:(NSString *)thread_id post_content:(NSString *)contents longitude:(NSString *)geo_longitude latitude:(NSString *)geo_latitude user_ids:(NSString *)tag_user_ids;
-(NSString *) thread_details:(NSString *)thread_id;

@end
