//
//  ApplicationTabbarController.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/3/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "ApplicationTabbarController.h"

@implementation ApplicationTabBarController

#pragma mark -
#pragma mark View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		NSMutableArray *localControllers = [[NSMutableArray alloc] initWithCapacity:5];
		
        //FeedTab controller
        UINavigationController *fifthNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		UIViewController *dummyViewController3 = [[MainScreen alloc] initWithNibName:nil bundle:nil];
		[dummyViewController3.view setBackgroundColor:[UIColor blackColor]];
		[fifthNavigationController pushViewController:dummyViewController3 animated:NO];
		[[fifthNavigationController tabBarItem] setTitle:@"Board"];
        //fifthNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Feed.png"];
        fifthNavigationController.tabBarItem.image = [UIImage imageNamed:@""];
		[localControllers addObject:fifthNavigationController];
		[dummyViewController3 release];
        //[fifthNavigationController setTitle: @"Feed"];
		[fifthNavigationController release];
		
		// TownTab Contoller
		UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		UIViewController *dummyViewController = [[LoginViewController alloc] initWithNibName:nil bundle:nil];
		[dummyViewController.view setBackgroundColor:[UIColor blackColor]];
		[secondNavigationController pushViewController:dummyViewController animated:NO];
		[[secondNavigationController tabBarItem] setTitle:@"Team"];
        //secondNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Towns.png"];
        secondNavigationController.tabBarItem.image = [UIImage imageNamed:@""];
		[localControllers addObject:secondNavigationController];
		[dummyViewController release];
		[secondNavigationController release];
        
        // Create first navigation controller, add view holding calendar, 
        // assign title to navigation controller, add to local controllers array
		UINavigationController *firstNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		MainScreen *mainScreen = [[MainScreen alloc] initWithNibName:nil bundle:nil];
		[firstNavigationController pushViewController:mainScreen animated:NO];
		[[firstNavigationController tabBarItem] setTitle:@"New Case"];
        firstNavigationController.tabBarItem.image = [UIImage imageNamed:@""];
		[localControllers addObject:firstNavigationController];
		[mainScreen release];
		[firstNavigationController release];
        //firstNavigationController.delegate= self;
        
        // FriendTab Controller
		UINavigationController *thirdNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		UIViewController *dummyViewController1 = [[MainScreen alloc] initWithNibName:nil bundle:nil];
		[dummyViewController1.view setBackgroundColor:[UIColor blackColor]];
		[thirdNavigationController pushViewController:dummyViewController1 animated:NO];
		[[thirdNavigationController tabBarItem] setTitle:@"Apps"];
        //thirdNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Friends.png"];
        thirdNavigationController.tabBarItem.image = [UIImage imageNamed:@""];
		[localControllers addObject:thirdNavigationController];
		[dummyViewController1 release];
		[thirdNavigationController release];
        
        
        
        
        // This is just a filler navigation controller for demo purposes
		UINavigationController *fourthNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		UIViewController *dummyViewController2 = [[MainScreen alloc] initWithNibName:nil bundle:nil];
		[dummyViewController2.view setBackgroundColor:[UIColor blackColor]];
		[fourthNavigationController pushViewController:dummyViewController2 animated:NO];
		[[fourthNavigationController tabBarItem] setTitle:@"Settings"];
        //fourthNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Deals.png"];
        fourthNavigationController.tabBarItem.image = [UIImage imageNamed:@""];
		[localControllers addObject:fourthNavigationController];
		[dummyViewController2 release];
		[fourthNavigationController release];
        
        
        
		// Add controllers to tab bar
		[self setViewControllers:localControllers animated:YES];
        //[self setRootViewController:firstNavigationController];
        
        [self setSelectedIndex:1];
        self.hidesBottomBarWhenPushed= YES;
		[localControllers release];
        
        self.navigationController.navigationBarHidden = YES;
        
        
        
        /* middle calendarButton 
         */
        //[self addCenterButtonWithImage:[UIImage imageNamed:@"calendarButton.png"] highlightImage:nil];
        //[TownTab release];
        
        
    }
    return self;
}

#pragma mark -
#pragma mark Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)dealloc {
    [super dealloc];
}

- (void)navigationController:(UINavigationController *)navController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([viewController respondsToSelector:@selector(willAppearIn:)])
        [viewController performSelector:@selector(willAppearIn:) withObject:navController];
}



// Create a custom UIButton and add it to the center of our tab bar
-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(selectCalendar) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    if (heightDifference < 0)
        button.center = self.tabBar.center;
    else
    {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0;
        button.center = center;
    }
    
    [self.view addSubview:button];
}

-(void)selectCalendar{
    [self setSelectedIndex:2];
}

@end
