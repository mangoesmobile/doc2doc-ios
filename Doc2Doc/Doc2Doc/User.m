//
//  User.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 29/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "User.h"

@implementation User

-(User *)init {
    return self;
}

-(NSString *) fetch_inbox:(NSString *)page_no{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/user/fetch_inbox"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:page_no forKey:@"page_no"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}


-(NSString *) all_vcards{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/user/all_vcards"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}


-(NSString *) get_vcard{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token= [prefs objectForKey:@"token"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/user/get_vcard"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request addPostValue:@"demo" forKey:@"user"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    NSString *returnString;
    
    if (!error) {
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"FALSE"]){
            
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
            
            NSLog(@"%@", returnString);
        }
    }
    else{
        NSLog(@"%@", error);
    }
    
    return returnString;
}


-(NSString *) my_vcard{
    return @"incomplete";
}
-(NSString *) set_profile_pic{
    return @"incomplete";
}
-(NSString *) set_user_availability_status{
    return @"incomplete";
}

@end
