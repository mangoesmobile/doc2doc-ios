//
//  Aunthenticate.m
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "Aunthenticate.h"

@implementation Aunthenticate


-(Aunthenticate *)init {
    
    return self;
   // NSLog(@"Test");
}

- (NSString *) loginId:(NSString *)login password:(NSString *)password{
    
    //SHA1 Encryption code
    NSString *hashkey = password;
    const char *s = [hashkey cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
    uint8_t result[CC_SHA1_DIGEST_LENGTH] = {0};
    CC_SHA1(keyData.bytes, keyData.length, result);
    //SHA1 Encryption code

    NSString *pass = [NSString  stringWithFormat:
                   @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                   result[0], result[1], result[2], result[3], result[4],
                   result[5], result[6], result[7],
                   result[8], result[9], result[10], result[11], result[12],
                   result[13], result[14], result[15],
                   result[16], result[17], result[18], result[19]
                   ];
    
    NSString *returnString;
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/authenticate/login"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue: login forKey:@"uname"];
    [request addPostValue: [pass lowercaseString ] forKey:@"enc_pword"];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    if (!error) {
        NSString *response = [request responseString];
        //NSLog(@"%@", response);
        
        //NSLog(@"%@",response);
        
        if (![response isEqualToString:@"FALSE"]) {
            NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
            [defaults setObject:response forKey:@"token"];
            
            returnString= [NSString stringWithFormat:@"%@", response];
        }
        else{
            
            returnString= [NSString stringWithFormat:@"%@", @"FALSE"];
        }
        
    }
    
    //NSLog(@"%@ , %@", login, password);
    
    //NSLog(@"Token found %@", returnString);
    
    return returnString;
}

-( NSString *) logOut{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *token=[prefs objectForKey:@"token"];
    
    NSURL *url = [NSURL URLWithString:@"http://50.18.120.168/doc2doc/index.php/authenticate/logout"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:token forKey:@"access_token"];
    [request startSynchronous];
    NSError *error = [request error];
    NSString *response;
    
    if (!error) {
       response  = [request responseString];
        
    }
    return  response;
}

- (IBAction)sendRe:(id)sender
{
    
}

@end
