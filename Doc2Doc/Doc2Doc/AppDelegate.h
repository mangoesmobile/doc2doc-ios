//
//  AppDelegate.h
//  Doc2Doc
//
//  Created by Mahbub Morhsed on 27/2/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Aunthenticate.h"
#import "Thread.h"
#import "User.h"
#import "ApplicationTabbarController.h"

#import "LoginViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


@property(nonatomic, retain) ApplicationTabBarController *tabBarController;
@property(nonatomic, retain) LoginViewController *login;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
